
Building the plotter
####################


Electrical parts
****************

- 1 x ESP32-S module |ESP32 module|
- 1 x Custom PCB (assembled) |custom PCB|
- 1 x 5-6 V power supply |power supply|
- 1 x 6-pin micro SD card holder |SD card holder|
- 1 x micro SD card |micro SD card|

.. |SD card holder| image:: images/sd-card-holder.jpg
    :alt: SD card holder
    :width: 120px

.. |custom PCB| image:: images/custom-pcb.png
    :alt: Custom PCB
    :width: 180px

.. |power supply| image:: images/power-supply.jpg
    :alt: DC power supply
    :width: 120px

.. |ESP32 module| image:: images/esp32.jpg
    :alt: ESP32-S module
    :width: 120px


.. |micro SD card| image:: images/micro-sd-card.jpg
    :alt: micro SD card
    :width: 120px

**N.B.:** There are different types of ESP32 module available. For this project a version which has 15-pin connectors is required. Please see image attached and ensure that your module has the same pin names.

3D printed parts
****************

The following parts can be printed using a 3D printer.

- 1 x plotter frame |plotter frame| `STL <https://designtrail.net/u/wall-plotter/stls/ver1/plotter.stl>`_
- 2 x spool |thread spool| `STL <https://designtrail.net/u/wall-plotter/stls/ver1/spool.stl>`_
- 2 x slide-rail |slide rail| `STL <https://designtrail.net/u/wall-plotter/stls/ver1/slide-rail.stl>`_
- 2 x servo arm link |servo arm link| `STL <https://designtrail.net/u/wall-plotter/stls/ver1/servo-arm-link.stl>`_
- 1 x spool clamp |spool clamp| `STL <https://designtrail.net/u/wall-plotter/stls/ver1/spool-clamp.stl>`_
- 1 x foot / stabilizer |stabilizer| `STL <https://designtrail.net/u/wall-plotter/stls/ver1/stabilizer.stl>`_

.. |plotter frame| image:: images/printed/frame.png
    :alt: Plotter frame
    :width: 180px

.. |thread spool| image:: images/printed/spool.png
    :alt: Thread spool
    :width: 120px

.. |slide rail| image:: images/printed/slide-rail.png
    :alt: Slide rail
    :width: 150px

.. |servo arm link| image:: images/printed/servo-arm-link.png
    :alt: Servo arm link
    :width: 120px

.. |spool clamp| image:: images/printed/spool-clamp.png
    :alt: Spool clamp
    :width: 180px


.. |stabilizer| image:: images/printed/stabilizer.png
    :alt: Stabilizer
    :width: 180px


All of the relevant STL files can be downloaded in a ZIP file at `<https://designtrail.net/u/wall-plotter/stls/ver1/stringent-printed-parts.zip>`_.

The parts are color coded to help you identify them in the images below - which shows where each printed part belongs in the final assembled device.

.. image:: images/printed/assembly-1.png
    :alt: Assembled plotter
    :width: 240px
    :align: center

.. image:: images/printed/assembly-2.png
    :alt: Assembled plotter
    :width: 240px
    :align: center

.. image:: images/printed/assembly-3.png
    :alt: Assembled plotter
    :width: 240px
    :align: center



Motors
******

- 2 x 28BYJ-48 stepper motor |stepper motor|
- 2 x SG90 servo motor |servo motor|

.. |stepper motor| image:: images/stepper-motor.jpeg
    :alt: Stepper motor
    :width: 120px

.. |servo motor| image:: images/servo-motor.jpg
    :alt: Servo motor
    :width: 120px


Mechanical (screws and nuts)
****************************

- M3 screws and nuts - approx. 16 (length not critical but ~10 mm will work)
- 2 x M2 or M2.5 screws for servo motors - length approx. 5 mm

3D printed parts 



