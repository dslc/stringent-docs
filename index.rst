.. wall_plotter documentation master file, created by
   sphinx-quickstart on Tue Sep 29 17:18:17 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Stringent wall plotter
======================

Contents:

.. toctree::
   :maxdepth: 2

   building

This is a version of Fredrik Stridsman's wall plotter for the ESP32.

The device is a 'vertical' or 'wall' plotter which allows you to plot SVG files on paper.

Fredrik's original version of the software parsed the SVG files directly in the Arduino firmware. This version instead uses Gcode files - so it is necessary to convert the SVG files to Gcode first (e.g. using the converter at http://jscut.org/jscut.html .)

The plotter can be controlled using a Bluetooth mobile app.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

